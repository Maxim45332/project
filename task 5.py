class Person:
    def __init__(self, firstname, lastname, surname):
        self.firstname = firstname
        self.lastname = lastname
        self.surname = surname

def __str__(self):
    return f'{self.lastname} {self.firstname} {self.surname}'

class Student(Person):
    def go_to_class(self, subject):
        print(f'Школьник {self.lastname} {self.firstname} {self.surname} идет на {subject}')

class Teacher(Person):
    def teach_class(self, subject):
        print(f'Учитель {self.lastname} {self.firstname} {self.surname} преподает {subject}')

student = Student('Иван', 'Иванов', 'Иванович')
teacher = Teacher('Петр', 'Петров', 'Эдуардович')

student.go_to_class('математику')
teacher.teach_class('информатику')