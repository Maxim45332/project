class User:
    def __init__(self, login, password):
        self.login = login
        self.password = password

    def is_strong_password(self):
        return len(self.password) > 8 and self.password != self.login


short_pswd = User('user', 'qwerty')
simple_pwsd = User('super_user', 'super_user')
strong_pswd = User('admin', 'Qwerty12345')
print(short_pswd.is_strong_password())
print(simple_pwsd.is_strong_password())
print(strong_pswd.is_strong_password())