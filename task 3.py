class NobelWinner:
    def __init__(self, winner, year, category):
        self.winner = winner
        self.year = year
        self.category = category

    def __str__(self):
        return f'{self.winner} выиграл нобелевскую премию по {self.category} в {self.year}'


winner = NobelWinner('Роджер Пенроуз', 2020, 'физика')
print(winner)